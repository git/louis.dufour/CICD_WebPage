<?php
/**
 * Auteur : Cédric BOUHOURS
 * Ce code est mis à disposition selon les termes de la Licence Creative Commons Attribution
 * Pas d'Utilisation Commerciale - Pas de Modification 4.0 International.
 *
 * Attribution — Vous devez créditer l'Œuvre, intégrer un lien vers la licence. Vous devez indiquer ces informations par tous les moyens raisonnables, sans toutefois suggérer que l'Offrant vous soutient ou soutient la façon dont vous avez utilisé son Oeuvre.
 *
 * Pas d’Utilisation Commerciale — Vous n'êtes pas autorisé à faire un usage commercial de cette Oeuvre, tout ou partie du matériel la composant.
 *
 * Pas de modifications — Dans le cas où vous effectuez un remix, que vous transformez, ou créez à partir du matériel composant l'Oeuvre originale, vous n'êtes pas autorisé à distribuer ou mettre à disposition l'Oeuvre modifiée.
 *
 * Pas de restrictions complémentaires — Vous n'êtes pas autorisé à appliquer des conditions légales ou des mesures techniques qui restreindraient légalement autrui à utiliser l'Oeuvre dans les conditions décrites par la licence.
 *
 *
 */


if (isErreur()) {
    require_once 'Traitement/alerts.php';
}

setAttachment($template, "chercher", "chercher.html");

if (isset ($_SESSION["meslivres_resultat_recherche"]) && isset($_GET['bfmw_num_index']) && isset($_GET['bfmw_orig_champ']) && isset($_GET['bfmw_orig_valeur'])) {
    openBloc($template,"resultat");

    affectToBlocAndRepeat($template,"resultat.un_livre",$_SESSION["meslivres_resultat_recherche"]["ITEM2"]??[]);

    affectToXHTML($template,array(
        "INDEX_CURRENT"=>$_GET['bfmw_num_index']-5,
        "INDEX_NEXT"=>$_GET['bfmw_num_index'],
        "INDEX_PREV"=>($_GET['bfmw_num_index']-10)>0?($_GET['bfmw_num_index']-10):0,
        "CHAMP"=>$_GET['bfmw_orig_champ'],
        "VALEUR"=>$_GET['bfmw_orig_valeur']
    ));

}


generateCompleteXHTML($template, "chercher");