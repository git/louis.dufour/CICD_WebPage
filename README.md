# WebPage

This project proposes a website allowing you to manage your library. It allows you to search for books in [OpenLibrary](https://openlibrary.org/), add them to your own library and manage loans.

## Getting Started

If you want to test this project locally, simply ```git clone``` this project on a apache/php8 system

## Running the tests

You can run some unit tests but there are few. So, you must use [phpUnit](https://phpunit.de/).

Tests are in common/Tests

In the context of continous integration, you have to deploy phpUnit. Next, simply execute tests on a php8 image with commands like : 

_vendor/bin/phpunit --filter "/([METHOD_TO_TEST])( .*)?$/"  [path to the test classes]_

You can find more details in the [Drone documentation](https://docs.drone.io/)

## Authors
Cédric Bouhours

## Acknowledgements
Marc Chevaldonné and Camille Petitalot