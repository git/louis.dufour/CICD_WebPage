FROM php:8.0-apache
EXPOSE 80
COPY . /var/www/html
WORKDIR /var/www/html
CMD [ "apache2-foreground" ]
