<?php

class WS_Util
{
    public static function CallWebService(string $base_url,string $endpoint, string $method,array $queryParams = [], $requestBody = [], string $login = "", string $pwd = "" ) : array {
        $url = $base_url . $endpoint;

        $options = [
            'http' => [
                'method' => $method,
                'header' => array(
                    'Content-Type: application/json',
                    empty($login)?:'Authorization: Basic $login $pwd'
                ),
                'ignore_errors' => true
            ]
        ];

        if (!empty($queryParams)) {
            $url .= '?' . http_build_query($queryParams);
        }

        if (!empty($requestBody)) {
            $options['http']['content'] = json_encode($requestBody);
        }

        $context = stream_context_create($options);

        $response = file_get_contents($url, false, $context);

        $data = json_decode($response, true);

        $retour = [];
        $retour = self::AllKeysToUppercase($data);

        return $retour;
    }

    public static function AllKeysToUppercase(?array $data): array
    {
        $retour = [];
        foreach ($data??[] as $cle => $valeur) {
            $cleMajuscules = strtoupper($cle);

            if (is_array($valeur)) {
                $retour[$cleMajuscules] = self::AllKeysToUppercase($valeur);
            } else {
                $retour[$cleMajuscules] = $valeur;
            }
        }
        return $retour;
    }
}


