<?php
use PHPUnit\Framework\TestCase;

include_once __DIR__."/../Model/WS_Util.php";
include_once __DIR__."/../../common.php";


class WS_UtilTest extends TestCase
{
    public function testCallWebService()
    {
        $base_url = BASE_URL_LIBRARY;
        $endpoint = GET_BOOKS_BY_TITLE;
        $method = 'GET';
        $queryParams = ['title' => 'a', 'index' =>  '0', 'count' => 5];
        $requestBody = [];
        $login = "";
        $pwd = "";

        $response = WS_Util::CallWebService($base_url, $endpoint, $method, $queryParams, $requestBody, $login, $pwd);
        $this->assertIsArray($response);
    }

    public function testAllKeysToUppercase()
    {
        $data = [
            'name' => 'John',
            'age' => 30,
            'address' => [
                'city' => 'Paris',
                'country' => 'France'
            ]
        ];

        $result = WS_Util::AllKeysToUppercase($data);

        $this->assertIsArray($result);
        $this->assertArrayHasKey('NAME', $result);
        $this->assertArrayHasKey('AGE', $result);
        $this->assertArrayHasKey('ADDRESS', $result);
        $this->assertArrayHasKey('CITY', $result['ADDRESS']);
        $this->assertArrayHasKey('COUNTRY', $result['ADDRESS']);
    }
}
