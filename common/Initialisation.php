<?php
/**
 * Auteur : Cédric BOUHOURS
 * Ce code est mis à disposition selon les termes de la Licence Creative Commons Attribution
 * Pas d'Utilisation Commerciale - Pas de Modification 4.0 International.
 *
 ant vous soutient ou soutient la façon dont vous avez utilisé son Oeuvre.
 *
 * Pas d’Utilisation Commerciale — Vous n'êtes pas autorisé à faire un usage commercial de cette Oeuvre, tout ou partie du matériel la composant.
 *
 * Pas de modifications — Dans le cas où vous effectuez un remix, que vous transformez, ou créez à partir du matériel composant l'Oeuvre originale, vous n'êtes pas autorisé à distribuer ou mettre à disposition l'Oeuvre modifiée.
 *
 * Pas de restrictions complémentaires — Vous n'êtes pas autorisé à appliquer des conditions légales ou des mesures techniques qui restreindraient légalement autrui à utiliser l'Oeuvre dans les conditions décrites par la licence.
 *
 *
 */

function isRegistered() : bool
{
    global $clef_session;
    return (CURRENT_USE != '' && CURRENT_ID != 0 && isset($_SESSION[$clef_session]));
}

function verifSession() : void
{
    if (!isRegistered()) {
        echo "Accès refusé.";
        exit;
    }
}

date_default_timezone_set("Europe/Paris");

define('CURRENT_USE', "");
require_once('self_server_concrete.php');
require_once("bfmw/includes/common.php");

if (isset($_GET['bfmw_orig_p'])) {
    if ($_GET['bfmw_orig_p'] === "logout") {
        require_once("Traitement/logout.php");
        exit;
    }
}

require_once 'common.php';

foreach ($required as $un_require) {
    require_once($un_require);
}

if (! isset($_GET['bfmw_orig_p'])) {
    $_GET['bfmw_orig_p'] = "accueil";
}
@session_start();

$_SESSION[$page_session] = $_GET['bfmw_orig_p'];

if (!file_exists("Traitement/" . $_GET['bfmw_orig_p'] . ".php")) {
    $_SESSION[$page_session] = "accueil";
} else {
    $_SESSION[$page_session] = $_GET['bfmw_orig_p'];
}

require_once 'Traitement/overall_header.php';

require_once 'Traitement/menu.php';

require_once("Traitement/" . $_SESSION[$page_session] . ".php");

require_once 'Traitement/overall_footer.php';