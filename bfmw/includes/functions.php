<?php
/**
 * Auteur : Cédric BOUHOURS
 * Ce code est mis à disposition selon les termes de la Licence Creative Commons Attribution
 * Pas d'Utilisation Commerciale - Pas de Modification 4.0 International.
 *
 * Attribution — Vous devez créditer l'Œuvre, intégrer un lien vers la licence. Vous devez indiquer ces informations par tous les moyens raisonnables, sans toutefois suggérer que l'Offrant vous soutient ou soutient la façon dont vous avez utilisé son Oeuvre.
 *
 * Pas d’Utilisation Commerciale — Vous n'êtes pas autorisé à faire un usage commercial de cette Oeuvre, tout ou partie du matériel la composant.
 *
 * Pas de modifications — Dans le cas où vous effectuez un remix, que vous transformez, ou créez à partir du matériel composant l'Oeuvre originale, vous n'êtes pas autorisé à distribuer ou mettre à disposition l'Oeuvre modifiée.
 *
 * Pas de restrictions complémentaires — Vous n'êtes pas autorisé à appliquer des conditions légales ou des mesures techniques qui restreindraient légalement autrui à utiliser l'Oeuvre dans les conditions décrites par la licence.
 *
 *
 */

function makeErreur($complement, $titre, $message)
{
    /**
     * "" = error
     * "success" = ok
     * "info" = info
     * "warning" = warning
     */
    global $_ERREUR;
    array_push($_ERREUR, array(
        "COMPLEMENT" => $complement,
        "TITRE" => $titre,
        "MESSAGE" => $message
    ));

}

function isErreur()
{
    global $_ERREUR;
    return (count($_ERREUR, COUNT_RECURSIVE) > 0 || (isset($_GET['bfmw_orig_cpl']) && isset($_GET['bfmw_orig_ttr']) && isset($_GET['bfmw_orig_mss'])));
}

function transformDate($date) {
    if (strpos($date,"/") !== false) {
        list($jour, $mois, $annee) = explode('/', $date);
    } else {
        if (strpos($date, "-") !== false) {
            list($annee, $mois, $jour) = explode('-', $date);
        }
    }

    return mktime(0,0,0,$mois,$jour,$annee);
}
function transformDate2359($date) {
    if (strpos($date,"/") !== false) {
        list($jour, $mois, $annee) = explode('/', $date);
    } else {
        if (strpos($date, "-") !== false) {
            list($annee, $mois, $jour) = explode('-', $date);
        }
    }
    return mktime(23,59,59,$mois,$jour,$annee);
}

function transformHeure($heure) {
    if (strpos($heure,"h") !== false) {
        list($hour, $minutes) = explode('h', $heure);
    } else {
        list($hour, $minutes) = explode(':', $heure);
    }
    return mktime($hour,$minutes,0,null,null,null);
}

function transformDateHeureSepares($date,$heure,$separateur_horaire = "h") {
    if (strpos($date,"/") !== false) {
        list($jour, $mois, $annee) = explode('/', $date);
    } else {
        if (strpos($date, "-") !== false) {
            list($annee, $mois, $jour) = explode('-', $date);
        }
    }
    list($heure, $minutes) = explode($separateur_horaire, $heure);
    return mktime($heure,$minutes,0,$mois,$jour,$annee);
}

function transformDateHeure($complet) {
    if (strpos($complet,"T") !== false) {
        list($date, $heure) = explode('T', $complet);
    } else {
        list($date, $heure) = explode(' ', $complet);
    }
    if (strpos($date,"/") !== false) {
        list($jour, $mois, $annee) = explode('/', $date);
    } else {
        if (strpos($date, "-") !== false) {
            list($annee, $mois, $jour) = explode('-', $date);
        }
    }
    if (strpos($heure,"h") !== false) {
        list($heure, $minutes) = explode('h', $heure);
    } else {
        list($heure, $minutes) = explode(':', $heure);
    }
    return mktime($heure,$minutes,0,$mois,$jour,$annee);
}

function extractTime(int $complet) : int {
    $converteur = new DateTime();
    $converteur->setTimestamp($complet);
    $converteur->setDate(1999,11,30);
    return $converteur->getTimestamp();
}

function extractDate(int $complet) : int {
    $converteur = new DateTime();
    $converteur->setTimestamp($complet);
    $converteur->setTime(0,0,0);
    return $converteur->getTimestamp();
}

function convertTimeStampToString(string $format,int $timestamp) : string {
    $date = new DateTime();
    $date->setTimestamp($timestamp);
    return $date->format($format);
}

set_error_handler('exceptions_error_handler');

function exceptions_error_handler($severity, $message, $filename, $lineno) {
    if (error_reporting() == 0) {
        return;
    }
    if (error_reporting() & $severity) {
        throw new ErrorException($message, 0, $severity, $filename, $lineno);
    }
}